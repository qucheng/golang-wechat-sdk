package context

import (
	"gitee.com/qucheng/golang-wechat-sdk/openplatform/config"
)

// Context struct
type Context struct {
	*config.Config
}
