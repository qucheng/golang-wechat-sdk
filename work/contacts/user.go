package contacts

import (
	"encoding/json"
	"fmt"
	"gitee.com/qucheng/golang-wechat-sdk/util"
	"gitee.com/qucheng/golang-wechat-sdk/work/context"
)

const (
	addUserURL  = "https://qyapi.weixin.qq.com/cgi-bin/user/create?access_token=%s"        // 创建成员
	userInfoURL = "https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token=%s&userid=%s" // 获取成员信息
)

// User 成员管理
type User struct {
	*context.Context
}

// NewUser 实例化成员管理
func NewUser(context *context.Context) *User {
	user := new(User)
	user.Context = context
	return user
}

// Info 成员基本信息
type Info struct {
	ErrCode          int             `json:"errcode"`
	ErrMsg           string          `json:"errmsg"`
	Userid           string          `json:"userid"`
	Name             string          `json:"name"`
	Department       []int           `json:"department"`
	Order            []int           `json:"order"`
	Position         string          `json:"position"`
	Mobile           string          `json:"mobile"`
	Gender           string          `json:"gender"`
	Email            string          `json:"email"`
	IsLeaderInDept   []int           `json:"is_leader_in_dept"`
	Avatar           string          `json:"avatar"`
	ThumbAvatar      string          `json:"thumb_avatar"`
	Telephone        string          `json:"telephone"`
	Alias            string          `json:"alias"`
	Address          string          `json:"address"`
	OpenUserid       string          `json:"open_userid"`
	MainDepartment   int             `json:"main_department"`
	ExtAttr          Extattr         `json:"extattr"` // 扩展属性，第三方仅通讯录应用可获取；对于非第三方创建的成员，第三方通讯录应用也不可获取
	Status           int             `json:"status"`
	QrCode           string          `json:"qr_code"`
	ExternalPosition string          `json:"external_position"`
	ExternalProfile  ExternalProfile `json:"external_profile"` // 成员对外属性，字段详情见对外属性；第三方仅通讯录应用可获取；对于非第三方创建的成员，第三方通讯录应用也不可获取
	Enable           int             `json:"enable"`           // 启用/禁用成员。1表示启用成员，0表示禁用成员
	HideMobile       int             `json:"hide_mobile"`      // 是否隐藏手机 0-显示 1-隐藏
	Isleader         int             `json:"isleader"`         // 是否是领导 0: 不是 1: 是
}

// Extattr 扩展字段
type Extattr struct {
	Attrs []Attrs `json:"attrs"`
}

type ExternalProfile struct {
	ExternalCorpName string         `json:"external_corp_name"`
	WechatChannels   WechatChannels `json:"wechat_channels"` // 视频号
	ExternalAttr     []ExternalAttr `json:"external_attr"`
}

// WechatChannels 视频号
type WechatChannels struct {
	Nickname string `json:"nickname"`
	Status   int    `json:"status"`
}

type Attrs struct {
	Type int    `json:"type"`
	Name string `json:"name"`
	Text Text   `json:"text"`
	Web  Web    `json:"web"`
}

type Text struct {
	Value string `json:"value"`
}

type Web struct {
	Url   string `json:"url"`
	Title string `json:"title"`
}

type MiniProgram struct {
	AppId    string `json:"appid"`
	Title    string `json:"title"`
	PagePath string `json:"pagepath"`
}

type ExternalAttr struct {
	Type        int         `json:"type"`
	Name        string      `json:"name"`
	Text        Text        `json:"text"`
	Web         Web         `json:"web"`
	MiniProgram MiniProgram `json:"miniprogram"`
}

// GetUserInfo 添加成员
func (user *User) GetUserInfo(userId string) (userInfo *Info, err error) {
	var accessToken string
	accessToken, err = user.GetAccessToken()
	if err != nil {
		return
	}
	uri := fmt.Sprintf(userInfoURL, accessToken, userId)
	var response []byte
	response, err = util.HTTPGet(uri)
	if err != nil {
		return
	}

	fmt.Println("====== GetUserInfo ======")
	fmt.Println(string(response))

	userInfo = new(Info)
	err = json.Unmarshal(response, &userInfo)
	if err != nil {
		return
	}

	if userInfo.ErrCode != 0 {
		err = fmt.Errorf("work GetUserInfo Error , errcode=%d , errmsg=%s", userInfo.ErrCode, userInfo.ErrMsg)
		return
	}
	return
}
