package work

import (
	"gitee.com/qucheng/golang-wechat-sdk/credential"
	"gitee.com/qucheng/golang-wechat-sdk/work/config"
	"gitee.com/qucheng/golang-wechat-sdk/work/contacts"
	"gitee.com/qucheng/golang-wechat-sdk/work/context"
	"gitee.com/qucheng/golang-wechat-sdk/work/js"
	"gitee.com/qucheng/golang-wechat-sdk/work/oauth"
)

// Work 微信公众号相关API
type Work struct {
	ctx *context.Context
}

// NewWork 实例化企业微信 API
func NewWork(cfg *config.Config) *Work {
	defaultAkHandle := credential.NewDefaultWorkAccessToken(cfg.CorpId, cfg.Secret, cfg.Cache)

	ctx := &context.Context{
		Config:                cfg,
		WorkAccessTokenHandle: defaultAkHandle,
	}
	return &Work{ctx}
}

// SetAccessTokenHandle 自定义access_token获取方式
func (work *Work) SetAccessTokenHandle(accessTokenHandle credential.WorkAccessTokenHandle) {
	work.ctx.WorkAccessTokenHandle = accessTokenHandle
}

// GetContext get Context
func (work *Work) GetContext() *context.Context {
	return work.ctx
}

// GetAccessToken 获取access_token
func (work *Work) GetAccessToken() (string, error) {
	return work.ctx.GetAccessToken()
}

// GetJs js-sdk配置
func (work *Work) GetJs() *js.Js {
	return js.NewJs(work.ctx)
}

// GetOauth 网页授权登录
func (work *Work) GetOauth() *oauth.Oauth {
	return oauth.NewOauth(work.ctx)
}

// GetUser 用户管理接口
func (work *Work) GetUser() *contacts.User {
	return contacts.NewUser(work.ctx)
}
