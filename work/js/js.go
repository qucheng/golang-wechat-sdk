package js

import (
	"fmt"
	"gitee.com/qucheng/golang-wechat-sdk/credential"
	"gitee.com/qucheng/golang-wechat-sdk/util"
	"gitee.com/qucheng/golang-wechat-sdk/work/context"
)

type Js struct {
	*context.Context
	credential.WorkJsTicketHandle
}

type Config struct {
	CorpId    string `json:"corp_id"`  // 企业ID
	AgentId   string `json:"agent_id"` // 应用ID
	Timestamp int64  `json:"timestamp"`
	NonceStr  string `json:"nonce_str"`
	Signature string `json:"signature"`
}

func NewJs(context *context.Context) *Js {
	js := new(Js)
	js.Context = context
	jsTicketHandle := credential.NewDefaultWorkJsTicket(context.CorpId, context.AgentId, credential.CacheKeyWorkPrefix, context.Cache, context.IsCorp)
	//return js
	js.SetJsTicketHandle(jsTicketHandle)
	return js
}

func (js *Js) SetJsTicketHandle(ticketHandle credential.WorkJsTicketHandle) {
	js.WorkJsTicketHandle = ticketHandle
}

//GetConfig 获取jssdk需要的配置参数
//uri 为当前网页地址
func (js *Js) GetConfig(uri string) (config *Config, err error) {
	config = new(Config)
	var accessToken string
	accessToken, err = js.GetAccessToken()

	if err != nil {
		return
	}

	var ticketStr string

	ticketStr, err = js.GetTicket(accessToken)
	if err != nil {
		return
	}

	nonceStr := util.RandomStr(16)
	timestamp := util.GetCurrTS()
	str := fmt.Sprintf("jsapi_ticket=%s&noncestr=%s&timestamp=%d&url=%s", ticketStr, nonceStr, timestamp, uri)
	sigStr := util.Signature(str)

	config.CorpId = js.CorpId
	config.NonceStr = nonceStr
	config.Timestamp = timestamp
	config.Signature = sigStr
	return
}
