package oauth

import (
	"encoding/json"
	"fmt"
	"gitee.com/qucheng/golang-wechat-sdk/util"
	"gitee.com/qucheng/golang-wechat-sdk/work/context"
	"net/http"
	"net/url"
)

const (
	redirectOauthURL       = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=snsapi_base&state=%s#wechat_redirect" // 网页授权链接
	webAppRedirectOauthURL = "https://open.work.weixin.qq.com/wwopen/sso/qrConnect?appid=%s&agentid=&s&redirect_uri=%s&state=%s"                                          // 扫码登录授权链接
	userInfoURL            = "https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo?access_token=%s&code=%s"                                                               // 获取访问用户身份
)

// Oauth 保存用户授权信息
type Oauth struct {
	*context.Context
}

// NewOauth 实例化授权信息
func NewOauth(context *context.Context) *Oauth {
	auth := new(Oauth)
	auth.Context = context
	return auth
}

// GetRedirectURL 构造网页授权链接
func (oauth *Oauth) GetRedirectURL(appid, redirectUri, state string) (string, error) {
	urlStr := url.QueryEscape(redirectUri)
	return fmt.Sprintf(redirectOauthURL, appid, urlStr, state), nil
}

// GetWebAppRedirectURL 构造扫码登录链接
func (oauth *Oauth) GetWebAppRedirectURL(appid, redirectUri, state string) (string, error) {
	urlStr := url.QueryEscape(redirectUri)
	return fmt.Sprintf(webAppRedirectOauthURL, appid, urlStr, state), nil
}

// Redirect 跳转到网页授权
func (oauth *Oauth) Redirect(writer http.ResponseWriter, req *http.Request, appid, redirectURI, state string) error {
	location, err := oauth.GetRedirectURL(appid, redirectURI, state)
	if err != nil {
		return err
	}
	http.Redirect(writer, req, location, http.StatusFound)
	return nil
}

// UserId 获取用户信息
type UserId struct {
	util.CommonError
	UserId   string `json:"UserId"`   // 当用户为企业成员返回此字段
	OpenId   string `json:"OpenId"`   // 非企业成员授权时返回此字段
	DeviceId string `json:"DeviceId"` // 手机设备号(由企业微信在安装时随机生成，删除重装会改变，升级不受影响)
}

// GetUserId 获取用户在企业中ID
func (oauth *Oauth) GetUserId(accessToken, code string) (result UserId, err error) {

	urlStr := fmt.Sprintf(userInfoURL, accessToken, code)
	var response []byte

	response, err = util.HTTPGet(urlStr)
	if err != nil {
		return
	}

	err = json.Unmarshal(response, &result)
	if err != nil {
		return
	}

	if result.ErrCode != 0 {
		err = fmt.Errorf("GetUserInfo error : errcode=%v , errmsg=%v", result.ErrCode, result.ErrMsg)
		return
	}
	return
}
