package config

import "gitee.com/qucheng/golang-wechat-sdk/cache"

// Config config for 企业微信
type Config struct {
	CorpId         string `json:"corp_id"`          // 企业ID
	AgentId        string `json:"agent_id"`         // 应用ID
	Secret         string `json:"secret"`           // 应用秘钥
	EncodingAESKey string `json:"encoding_aes_key"` // EncodingAESKey
	Cache          cache.Cache
	IsCorp         bool `json:"is_crop"` // 获取 ticket 的方式，true: 获取应用 ticket
}
