package context

import (
	"gitee.com/qucheng/golang-wechat-sdk/credential"
	"gitee.com/qucheng/golang-wechat-sdk/miniprogram/config"
)

// Context struct
type Context struct {
	*config.Config
	credential.AccessTokenHandle
}
