package context

import (
	"gitee.com/qucheng/golang-wechat-sdk/credential"
	"gitee.com/qucheng/golang-wechat-sdk/officialaccount/config"
)

// Context struct
type Context struct {
	*config.Config
	credential.AccessTokenHandle
}
