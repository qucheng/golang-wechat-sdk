package credential

import (
	"encoding/json"
	"fmt"
	"gitee.com/qucheng/golang-wechat-sdk/cache"
	"gitee.com/qucheng/golang-wechat-sdk/util"
	"sync"
	"time"
)

const (
	// 企业的jsapi_ticket
	getWorkTicketURL = "https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token=%s"
	// 企业应用的jsapi_ticket
	getWorkAgentTicketURL = "https://qyapi.weixin.qq.com/cgi-bin/ticket/get?access_token=%s&type=agent_config"
)

type DefaultWorkJsTicket struct {
	corpId          string
	agentId         string
	cacheKeyPrefix  string
	cache           cache.Cache
	jsAPITicketLock *sync.Mutex //jsAPITicket 读写锁 同一个AppID一个
	isCorp          bool        // 是否获取企业 ticket
}

func NewDefaultWorkJsTicket(corpId, agentId string, cacheKeyPrefix string, cache cache.Cache, isCorp bool) WorkJsTicketHandle {
	return &DefaultWorkJsTicket{
		corpId:          corpId,
		agentId:         agentId,
		cacheKeyPrefix:  cacheKeyPrefix,
		cache:           cache,
		jsAPITicketLock: new(sync.Mutex),
		isCorp:          isCorp,
	}
}

type ResWorkTicket struct {
	util.CommonError
	Ticket    string `json:"ticket"`
	ExpiresIn int64  `json:"expires_in"`
}

func (js *DefaultWorkJsTicket) GetTicket(accessToken string) (ticketStr string, err error) {
	js.jsAPITicketLock.Lock()
	defer js.jsAPITicketLock.Unlock()

	var jsAPITicketCacheKey string
	// 先从 cache 获取
	if js.isCorp { // 企业 ticket
		jsAPITicketCacheKey = fmt.Sprintf("%s_jsapi_ticket_%s", js.cacheKeyPrefix, js.corpId)
	} else { // 应用 ticket
		jsAPITicketCacheKey = fmt.Sprintf("%s_jsapi_ticket_%s_%s", js.cacheKeyPrefix, js.corpId, js.agentId)
	}

	val := js.cache.Get(jsAPITicketCacheKey)
	if val != nil {
		ticketStr = val.(string)
		return
	}

	var ticket ResWorkTicket
	ticket, err = GetWorkTicketFromServer(accessToken, js.isCorp)
	if err != nil {
		return
	}

	expires := ticket.ExpiresIn - 1500
	err = js.cache.Set(jsAPITicketCacheKey, ticket.Ticket, time.Duration(expires)*time.Second)
	ticketStr = ticket.Ticket
	return
}

func GetWorkTicketFromServer(accessToken string, isCorp bool) (ticket ResWorkTicket, err error) {
	var response []byte

	var getTicketUrl string
	// 企业 ticket
	if isCorp {
		getTicketUrl = fmt.Sprintf(getWorkTicketURL, accessToken)
	} else { // 应用 ticket
		getTicketUrl = fmt.Sprintf(getWorkAgentTicketURL, accessToken)
	}

	response, err = util.HTTPGet(getTicketUrl)
	if err != nil {
		return
	}

	err = json.Unmarshal(response, &ticket)
	if err != nil {
		return
	}

	if ticket.ErrCode != 0 {
		err = fmt.Errorf("getTicket Error : errcode=%d , errmsg=%s", ticket.ErrCode, ticket.ErrMsg)
		return
	}
	return
}
