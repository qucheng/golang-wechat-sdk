package credential

// AccessTokenHandle AccessToken 接口
type AccessTokenHandle interface {
	GetAccessToken() (accessToken string, err error)
}

// WorkAccessTokenHandle WorkAccessToken 接口
type WorkAccessTokenHandle interface {
	GetAccessToken() (accessToken string, err error)
}
