package credential

//JsTicketHandle js ticket获取
type JsTicketHandle interface {
	//GetTicket 获取ticket
	GetTicket(accessToken string) (ticket string, err error)
}

// WorkJsTicketHandle 企业微信 js ticket 获取
type WorkJsTicketHandle interface {
	GetTicket(accessToken string) (ticket string, err error)
}
