package credential

import (
	"encoding/json"
	"fmt"
	"gitee.com/qucheng/golang-wechat-sdk/cache"
	"gitee.com/qucheng/golang-wechat-sdk/util"
	"sync"
	"time"
)

const (
	// workAccessTokenURL 获取access_token的接口
	workAccessTokenURL = "https://qyapi.weixin.qq.com/cgi-bin/gettoken"
	// CacheKeyWorkPrefix 企业微信 cache key 前缀
	CacheKeyWorkPrefix = "gowechat_work_"
)

//DefaultWorkAccessToken 默认AccessToken 获取
type DefaultWorkAccessToken struct {
	corpId          string
	AgentId         string
	secret          string
	cacheKeyPrefix  string
	cache           cache.Cache
	accessTokenLock *sync.Mutex
}

// WorkResAccessToken struct
type WorkResAccessToken struct {
	util.CommonError
	AccessToken string `json:"access_token"`
	ExpiresIn   int64  `json:"expires_in"`
}

// NewDefaultWorkAccessToken 创建企业微信 token
func NewDefaultWorkAccessToken(corpId, secret string, cache cache.Cache) WorkAccessTokenHandle {
	if cache == nil {
		panic("cache is i need")
	}

	return &DefaultWorkAccessToken{
		corpId:          corpId,
		secret:          secret,
		cacheKeyPrefix:  CacheKeyWorkPrefix,
		cache:           cache,
		accessTokenLock: new(sync.Mutex),
	}
}

//GetAccessToken 获取access_token,先从cache中获取，没有则从服务端获取
func (ak *DefaultWorkAccessToken) GetAccessToken() (accessToken string, err error) {
	//加上lock，是为了防止在并发获取token时，cache刚好失效，导致从微信服务器上获取到不同token
	ak.accessTokenLock.Lock()
	defer ak.accessTokenLock.Unlock()

	accessTokenCacheKey := fmt.Sprintf("%s_access_token_%s_%s", ak.cacheKeyPrefix, ak.corpId, ak.AgentId)
	val := ak.cache.Get(accessTokenCacheKey)

	if val != nil {
		accessToken = val.(string)
	}

	// cache 失效，从微信服务器重新获取
	var resAccessToken WorkResAccessToken
	resAccessToken, err = GetWorkTokenFromServer(ak.corpId, ak.secret)

	if err != nil {
		return
	}

	expires := resAccessToken.ExpiresIn - 1500
	err = ak.cache.Set(accessTokenCacheKey, resAccessToken.AccessToken, time.Duration(expires)*time.Second)
	if err != nil {
		return
	}

	accessToken = resAccessToken.AccessToken
	return
}

// GetWorkTokenFromServer 从微信服务器获取token
func GetWorkTokenFromServer(corpId, secret string) (resAccessToken WorkResAccessToken, err error) {
	url := fmt.Sprintf("%s?corpid=%s&corpsecret=%s", workAccessTokenURL, corpId, secret)
	var body []byte
	body, err = util.HTTPGet(url)
	if err != nil {
		return
	}

	err = json.Unmarshal(body, &resAccessToken)
	if err != nil {
		return
	}

	if resAccessToken.ErrCode != 0 {
		err = fmt.Errorf("get access_token error : errcode=%v , errormsg=%v", resAccessToken.ErrCode, resAccessToken.ErrMsg)
		return
	}
	return
}
